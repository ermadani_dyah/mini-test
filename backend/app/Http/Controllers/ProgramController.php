<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProgramController extends Controller
{
    public function Apabole()
    {
        $numbers = [];
        $start=1;
        $end=100;
        for($i=$start;$i<=$end;$i++){
            if($i%3==0){
                $numbers []='Apa';
            }else if($i%5==0){
                $numbers[]='Bole';
            }else if(($i%3==0)&&($i%5==0)){
                $numbers[]='ApaBole';
            }else{
                $numbers[]=$i;
            }
        }
        return $numbers;
    }
    public function Weather()
    {
        $apiKey = env('API_KEY');
        $city = 'Jakarta';
        $url = "http://api.openweathermap.org/data/2.5/forecast?q={$city}&appid={$apiKey}&units=metric";
        
        $response = Http::get($url);
    
        if ($response->successful()) {
            $data = $response->json();
            $forecasts = [];

            foreach ($data['list'] as $forecast) {
                $date = date('D,d M y', strtotime($forecast['dt_txt']));
                $temperature = $forecast['main']['temp_max'];

                if (!isset($forecasts[$date])) {
                    $forecasts[$date] = $temperature.' C';
                }
            }
            return $forecasts;
        } else {
            return 'Gagal mendapatkan data cuaca.';
        }
    }
}
